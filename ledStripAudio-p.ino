#define blueLED 9 // Red
#define redLED 10 // White
#define greenLED 11 // Red-Black

int beat;
int colour;
int intensity;
int light;

void setup() {
  pinMode(blueLED, OUTPUT);
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  analogWrite(blueLED, 255);
  analogWrite(redLED, 255);
  analogWrite(greenLED, 255);
  Serial.begin(9600);  
  }

void loop() {
  if (Serial.available()){
    beat = Serial.read();
  }
  if (beat == 1) {
    intensity = random(4);
    if (intensity == 0)
      light = 80;
    else if (intensity == 1)
      light = 5;
    else if (intensity == 2)
      light = 140;
    else if (intensity == 3)
      light = 40;
    else if (intensity == 4)
      light = 180;
    colour = random(6);
    if (colour == 2)
      analogWrite(blueLED, light);
    if (colour == 4)
      analogWrite(redLED, light);
    if (colour == 1)
      analogWrite(greenLED, light);
    if (colour == 5) {
      analogWrite(blueLED, light);
      analogWrite(greenLED, light);
    }
    if (colour == 3) {
      analogWrite(blueLED, light);
      analogWrite(redLED, light);
    }
    if (colour == 0) {
      analogWrite(redLED, light);
      analogWrite(greenLED, light);
    }
    if (colour == 6) {
      analogWrite(blueLED, light);
      analogWrite(redLED, light);
      analogWrite(greenLED, light);
    }
    beat = 0;
    delay(125);
    analogWrite(blueLED, 255);
    analogWrite(redLED, 255);
    analogWrite(greenLED, 255);
   
  } else {
    analogWrite(blueLED, 255);
    analogWrite(redLED, 255);
    analogWrite(greenLED, 255);
  }
}
