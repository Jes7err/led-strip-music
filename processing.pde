import processing.serial.*;
import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioInput mic;
BeatDetect beat;

int intensity = 1;
Serial port;

void setup() {
  size(10, 10);
  background(255);  
  
  port = new Serial(this, Serial.list()[0], 9600); 
  
  minim = new Minim(this);
  mic = minim.getLineIn();
  beat = new BeatDetect();
  ellipseMode(RADIUS);
}

void draw() {
  background(255);

  beat.detect(mic.mix);
  beat.setSensitivity(125);
  if (beat.isOnset()) {
    port.write(intensity);
    print("beat");
  }
}
